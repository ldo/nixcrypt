#+
# Python interface to crypt(3) functions from glibc/libcrypt.
#
# Copyright 2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import os
import ctypes as ct

#+
# Low-level definitions, for internal use
#-

libc = ct.cdll.LoadLibrary("libc.so.6")
libcrypt = ct.CDLL("libcrypt.so.1", use_errno = True)

libc.free.restype = None
libc.free.argtypes = (ct.c_void_p,)
libc.strlen.restype = ct.c_size_t
libc.strlen.argtypes = (ct.c_void_p,)
libc.memcpy.restype = None
libc.memcpy.argtypes = (ct.c_void_p, ct.c_void_p, ct.c_size_t)
libcrypt.crypt_ra.restype = ct.c_void_p
libcrypt.crypt_ra.argtypes = (ct.c_char_p, ct.c_char_p, ct.POINTER(ct.c_void_p), ct.POINTER(ct.c_int))
libcrypt.crypt_gensalt_ra.restype = ct.c_void_p
libcrypt.crypt_gensalt_ra.argtypes = (ct.c_char_p, ct.c_ulong, ct.c_void_p, ct.c_int)

#+
# Higher-level wrappers
#-

def crypt(phrase, setting) :
    "encrypts the plaintext phrase according to the setting" \
    " (algorithm prefix + salt). Both arguments can be of type" \
    " str, or they can be of type bytes."
    if sum(isinstance(phrase, t) and isinstance(setting, t) for t in (str, bytes, bytearray)) != 1 :
        raise TypeError("phrase and settings must both be str or bytes")
    #end if
    if isinstance(phrase, (bytes, bytearray)) : # both are bytes/bytearray
        c_phrase = phrase
        c_setting = setting
    else : # both are str
        c_phrase = phrase.encode()
        c_setting = setting.encode()
    #end if
    data = ct.c_void_p()
    datasize = ct.c_int()
    c_result = libcrypt.crypt_ra(c_phrase, c_setting, ct.byref(data), ct.byref(datasize))
    if c_result == None :
        errno = ct.get_errno()
        raise OSError(errno, os.strerror(errno))
    #end if
    resultlen = libc.strlen(c_result)
    result = bytes(resultlen)
    libc.memcpy(ct.cast(result, ct.c_void_p).value, c_result, resultlen)
    libc.free(data.value)
    if isinstance(phrase, str) :
        # return same type as passed
        result = result.decode()
    #end if
    return \
        result
#end crypt

def crypt_gensalt(prefix = None, count : int = 0, rbytes = None) :
    "generates a salt that can be used for hashing a password" \
    " according to a specified algorithm. The algorithm is selected" \
    " by prefix (which can be a str/bytes or None); count is a CPU" \
    " time cost parameter that is interpreted in an algorithm-dependent" \
    " manner; and rbytes is an optional bytes object containing some" \
    " suitable random data for computing the salt.\n" \
    "\n" \
    "For more details about the use of the parameters, see the crypt(5)" \
    " <https://manpages.debian.org/5/crypt.5.en.html> man page."
    if prefix == None :
        c_prefix = None
    elif isinstance(prefix, (bytes, bytearray)) :
        c_prefix = prefix
    elif isinstance(prefix, str) :
        c_prefix = prefix.encode()
    else :
        raise TypeError("prefix must be str or bytes")
    #end if
    if isinstance(rbytes, bytes) :
        c_rbytes = ct.cast(rbytes, ct.c_void_p).value
        c_nrbytes = len(rbytes)
    elif isinstance(rbytes, bytearray) :
        c_rbytes = ct.addressof((ct.c_ubyte * 1)).from_buffer(rbytes)
        c_nrbytes = len(rbytes)
    elif rbytes == None :
        c_rbytes = None
        c_nrbytes = 0
    else :
        raise TypeError("rbytes must be bytes or None")
    #end if
    if count == None :
        count = 0
    #end if
    c_result = libcrypt.crypt_gensalt_ra(c_prefix, count, c_rbytes, c_nrbytes)
    if c_result == None :
        errno = ct.get_errno()
        raise OSError(errno, os.strerror(errno))
    #end if
    resultlen = libc.strlen(c_result)
    result = bytes(resultlen)
    libc.memcpy(ct.cast(result, ct.c_void_p).value, c_result, resultlen)
    libc.free(c_result)
    if isinstance(prefix, str) :
        # return same type as passed
        result = result.decode()
    #end if
    return \
        result
#end crypt_gensalt
