#!/usr/bin/python3
#+
# Example use of nixcrypt: generate and check password hashes.
# This script can be invoked in the following ways:
#
#     cryptit [--prefix=«prefix»] [--count=«count»] [--rbytes=«rbytes»]
#
# («rbytes» being a base64-encoded string) to generate a salt.
#
#     cryptit [--prefix=«prefix»] [--count=«count»] [--rbytes=«rbytes»] «password»
#
# or
#
#     cryptit --salt=«salt» «password»
#
# to generate a password hash given a password and either a previously-generated
# salt or the parameters for generating a new one.
#
#     cryptit [-q] «password» «hash»
#
# to check whether the hash of «password» matches «hash». The exit status
# is 0 if they match, 1 if not; the phrase “valid/not valid” is also output,
# unless -q (quiet) is specified.
#-

import sys
import base64
import getopt
import nixcrypt as nx

count = None
prefix = None
rbytes = None
salt = None
quiet = False
opts, args = getopt.getopt \
  (
    sys.argv[1:],
    "q",
    ["count=", "prefix=", "rbytes=", "salt="]
  )
for keyword, value in opts :
    if keyword == "--count" :
        count = int(value)
    elif keyword == "-q" :
        quiet = True
    elif keyword == "--prefix" :
        prefix = value.encode()
    elif keyword == "--rbytes" :
        rbytes = base64.b64decode(value)
    elif keyword == "--salt" :
        salt = value
    #end if
#end for
status = 0
if len(args) == 0 :
    if salt != None :
        raise getopt.GetoptError("--salt not wanted for generating salt")
    #end if
    salt = nx.crypt_gensalt(prefix, count, rbytes)
    if isinstance(salt, bytes) :
        salt = salt.decode()
    #end if
    sys.stdout.write(salt)
    sys.stdout.write("\n")
elif len(args) == 1 :
    if salt != None and any(o != None for o in (count, prefix, rbytes)) :
        raise getopt.GetoptError("cannot specify both --salt and salt-generation options")
    #end if
    if salt == None :
        salt = nx.crypt_gensalt(prefix, count, rbytes).decode()
    #end if
    hash = nx.crypt(args[0], salt)
    sys.stdout.write(hash)
    sys.stdout.write("\n")
elif len(args) == 2 :
    if any(o != None for o in (count, prefix, rbytes, salt)) :
        raise getopt.GetoptError("hashing options not wanted for checking hash")
    #end if
    valid = nx.crypt(args[0], args[1]) == args[1]
    if not quiet :
        if not valid :
            sys.stdout.write("not ")
        #end if
        sys.stdout.write("valid\n")
    #end if
    status = (1, 0)[valid]
else :
    raise getopt.GetoptError("need 1 arg to hash, 2 args to check password")
#end if
sys.exit(status)
